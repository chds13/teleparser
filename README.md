# Parser for telegram groups

An application for obtaining information about Telegram groups and their participants. The result is placed in the file 'result.xlsx', a photo in the 'images' folder.

---

### How to use it

To begin, you must fill(insert your values instead <place>) the 'auth' file with the following:

* app_id <place>    *- application id, [more info](https://core.telegram.org/api/obtaining_api_id)*
* app_hash <place>  *- application hash, [more info](https://core.telegram.org/api/obtaining_api_id)*
* phone <place>     *- the phone number on which the authorization request will come*
* proxy <place>     *- optional value, you can specify SOCKS5 server address*
* port <place>      *- optional value, you can specify SOCKS5 server port*
* username <place>  *- optional value, you can specify SOCKS5 username*
* password <place>  *- optional value, you can specify SOCKS5 password*

Here is a example:  
	`app_id 123456`  
	`app_hash 12345abcd`  
	`phone +11111111111`  
	`proxy localhost`  
	`port 9050`  
	`username`  
	`password`

After saving the file, you can run the program as follows: "python ./teleparser.py <place your group link here>".

---

### Used libraries

* [Pillow](https://github.com/python-pillow/Pillow)
* [telethon-sync](https://github.com/LonamiWebs/Telethon)
* [xlsxwriter](https://github.com/jmcnamara/XlsxWriter)
* [PySocks](https://github.com/Anorov/PySocks)


